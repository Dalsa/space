<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Hyeon Toeic 에러</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<jsp:include page="/header.jsp" flush="true" />
	
	<div class = "container">
		<h2 class = "text-center">에러 발생</h2>
		<p class = "text-center">다시 시도해 주세요.</p>
		<div class = "text-center">
			<input type="button" value="첫페이지 " onclick="window.location.href='${pageContext.request.contextPath}/index.jsp';" />
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
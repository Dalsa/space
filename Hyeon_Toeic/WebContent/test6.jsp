<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import = "com.example.Toiec.model.score, com.example.Toiec.model.Id" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Hyeon Toeic 테스트</title>
	<link href="${pageContext.request.contextPath}/resource/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resource/js/bootstrap.min.js"></script>
</head>
<body>
	<%
		request.setCharacterEncoding("utf-8");
		Id id = (Id)session.getAttribute("id");
	%>
	<jsp:include page="/header.jsp" flush="true" />
	<br>
	<h4 class = "text-center"><%= id.getName() %>님 로그인 중 입니다.</h4>

	<div class = "container">
		<h3 class = "text-center">보카 시험 5</h3>
		<p class = "text-center">토익 문제입니다. 맞는 답을 찾아주세요.</p>
	</div>

	<form class = "form-horizontal" action = "testing6" method = "post">
	
		<div class = "container">
			<div class="col-sm-offset-2 col-sm-10">
				<span class = "text-center">13. '<%= session.getAttribute("question13") %>'이라는 뜻을 가진 영어 단어</span>
				<input type = "radio" name = "13" value = "1">foreword
				<input type = "radio" name = "13" value = "0">forward
			</div>
		</div>
		<br>
		<div class = "container">
			<div class="col-sm-offset-2 col-sm-10">
				<span class = "text-center">14. '<%= session.getAttribute("question14") %>'를 뜻하는 영어 단어</span>
				<input type = "radio" name = "14" value = "0">founder
				<input type = "radio" name = "14" value = "1">flounder
			</div>
		</div>
		<br>
		<div class = "container">
			<div class="col-sm-offset-2 col-sm-10">
				<span class = "text-center">15. '<%= session.getAttribute("question15") %>'를 뜻하는 영어 단어</span>
				<input type = "radio" name = "15" value = "1">disinterested
				<input type = "radio" name = "15" value = "0">uninterested
			</div>
		</div>
		<br>
		<div class="container">
    		<div class="col-sm-offset-2 col-sm-10">
     			<button type="submit" class="btn btn-default">성적보기</button>
    		</div>
  		</div>
	</form>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Hyeon Toeic 로그인</title>
	<link href="${pageContext.request.contextPath}/resource/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resource/js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="/header.jsp" flush="true" />
	
	<div class = "container">
		<h3 class = "text-center">로그인 페이지</h3>
		<p class = "text-center">로그인 뒤에 점수를 봅니다.</p>
	</div>

	<form class = "form-horizontal" action = "scoredo" method = "post">
	
		<div class = "container">
			<label for = "inputemail" class="col-sm-2 control-label">이메일</label>
			<div class = "col-sm-10">
				<input type = "text" name = "email" class="form-control" placeholder = "이메일을 입력하세요" >
			</div>
		</div>
		<br>
		
		<div class = "container">
			<label for ="inputpassword" class="col-sm-2 control-label">비밀번호</label>
			<div class = "col-sm-10">
				<input type = "password" name = "pwd" class = "form-control" id="pwd" placeholder = "비밀번호">
			</div>
		</div>
		<br>
		
		<div class="container">
    		<div class="col-sm-offset-2 col-sm-10">
     			<button type="submit" class="btn btn-default btn-lg btn-block">로그인 및 점수 보기</button>
    		</div>
  		</div>
	</form>
</body>
</html>
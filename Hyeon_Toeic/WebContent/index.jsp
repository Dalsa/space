<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<html>
<head>
	<meta charset="utf-8">
	<title>Hyeon Toeic</title>
	<link href="${pageContext.request.contextPath}/resource/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resource/js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="/header.jsp" flush="true" />
	
	<div class = "container">
		<h1 class = "text-center">간단 토익 테스트</h1>
		<p class = "text-center">회원가입을 해야 시험 응시가 가능한 토익 테스트 입니다.</p>
	</div>

	<div class = "container">
		<div class="col-md-6 col-md-offset-3">
     		<a class = "btn btn-default btn-lg btn-block" href = "op/test">시험 응시</a>
    	</div>
	</div>
	<br>
	
	<div class = "container">
		<div class="col-md-6 col-md-offset-3">
     		<a class = "btn btn-primary btn-lg btn-block" href = "op/register">회원 가입</a>
    	</div>
	</div>
	<br>
	
	<div class = "container">
		<div class="col-md-6 col-md-offset-3">
     		<a class = "btn btn-default btn-lg btn-block" href = "op/score">성적 확인</a>
    	</div>
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Hyeon Toeic 회원가입</title>
	<link href="${pageContext.request.contextPath}/resource/css/bootstrap.min.css" rel="stylesheet">
	<script>
		function isSame() {
			var pw = document.getElementById("pwd").value;
			var pw_1 = document.getElementById("confirm_pwd").value;
			if(confirm_pwd != "" && pw != pw_1) {
				alert("비밀번호가 일치 하지 않거나 비밀번호 확인을 입력하지 않았습니다.");
			}
		}
	</script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resource/js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="/header.jsp" flush="true" />
	
	<div class = "container">
		<h3 class = "text-center">회원가입</h3>
	</div>

	<form class = "form-horizontal" action = "register.do" method = "post">
	
		<div class = "container">
			<label for = "inputemail" class="col-sm-2 control-label">이메일 주소</label>
			<div class = "col-sm-10">
				<input type = "email" name = "email" class="form-control" id="inputemail" placeholder = "이메일을 입력하세요" >
			</div>
		</div>
		<br>
		
		<div class = "container">
			<label for ="inputpassword" class="col-sm-2 control-label">비밀번호</label>
			<div class = "col-sm-10">
				<input type = "password" name = "pwd" class = "form-control" id="pwd" placeholder = "비밀번호">
			</div>
		</div>
		<br>
		
		<div class = "container">
			<label for ="inputpassword1" class="col-sm-2 control-label">비밀번호 확인</label>
			<div class = "col-sm-10">
				<input type = "password" name = "pwd_r" class = "form-control" id="confirm_pwd" onblur="isSame()" placeholder = "비밀번호 확인">
			</div>
		</div>
		<br>
		
		<div class = "container">
			<label for="inputname" class="col-sm-2 control-label">이름</label>
			<div class = "col-sm-10">
				<input type = "text" name = "name" class = "form-control" id="inputname" placeholder = "이름을 입력해주세요.">
			</div>
		</div>
		<br>
		
		<div class = "container">
			<label for = "inputphone" class = "col-sm-2 control-label">휴대폰 번호</label>
			<div class = "col-sm-10">
				<input type = "text" name = "phone" class = "form-control" id="inputphone" placeholder = "휴대폰 번호를 번호만 입력해주세요" >
			</div>
		</div>
		<br>
		
		<div class="container">
    		<div class="col-sm-offset-2 col-sm-10">
     			<button type="submit" class="btn btn-default btn-lg btn-block">회원가입</button>
    		</div>
  		</div>
	</form>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import = "com.example.Toiec.model.Id" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Hyeon Toeic 로그인</title>
	<link href="${pageContext.request.contextPath}/resource/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resource/js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="/header.jsp" flush="true" />
	
	<%
		request.setCharacterEncoding("utf-8");
		Id id = (Id)session.getAttribute("id");
	%>
	
	<div class = "container">
		<h3 class = "text-center">점수 확인</h3>
		<p class = "text-center"><%= id.getName() %>님의 점수는 <%= id.getScore() %></p>
		<div class = "text-center">
			<input class = "btn btn-default" type="button" value="첫페이지 " onclick="window.location.href='${pageContext.request.contextPath}/index.jsp';" />
		</div>
	</div>
</body>
</html>
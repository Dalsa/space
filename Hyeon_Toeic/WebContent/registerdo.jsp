<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Hyeon Toeic 회원가입</title>
	<link href="${pageContext.request.contextPath}/resource/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resource/js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="/header.jsp" flush="true" />
	
	<div class = "container">
		<h3 class = "text-center">회원가입</h3>
	</div>
	<div class = "container">
		<h2 class = "text-center">회원가입이 완료되었습니다.</h2>
		<br>
		<div class = "text-center">
			<input class = "btn btn-default" type = "button" value ="첫페이지" onclick="window.location.href='${pageContext.request.contextPath}/index.jsp';">
		</div>
	</div>
</body>
</html>
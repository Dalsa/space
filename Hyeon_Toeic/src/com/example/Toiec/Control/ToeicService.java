package com.example.Toiec.Control;

import java.util.List;

import com.example.Toiec.model.DbConnectionInfo;
import com.example.Toiec.model.Id;
import com.example.Toiec.model.ToeicDbDao;
import com.example.Toiec.model.ToeicDbJdbcOrigDao;
import com.example.Toiec.model.score;

public class ToeicService {
	private ToeicDbDao dao = null;
	
	public ToeicService(DbConnectionInfo connInfo) {
		dao = new ToeicDbJdbcOrigDao(connInfo);
	}
	
	public Id searchId(String email, String pwd) { //id 찾는 기능
		Id id = null;
		
		if(email != null || email != "" || pwd != null || pwd != "") {
			List<Id> list = dao.getId(email, pwd);
			if(list != null) {
				id = list.get(0);
			}
		}
		return id;
	}
	
	public int registerId(Id id) { //회원가입 하는 기능
		int result = -1;
		
		if(id != null) {
			result = dao.registerId(id);
		}
		return result;
	}
	
	public score getQuestion(int test_id) {
		score score = null;
		
		if(test_id != 0) {
			List<score> list = dao.getTest(test_id);
			if(list != null) {
				score = list.get(0);
			}
		}
		return score;
	}
	
	public int updateScore(Id id) {
		int result = -1;
		
		if(id.getEmail() != null || id.getEmail() != "" || id.getScore() < 16 || id.getScore() < 0) {
			result = dao.updateTest(id);
		}
		return result;
	}
}

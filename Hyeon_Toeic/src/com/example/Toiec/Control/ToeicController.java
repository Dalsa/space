package com.example.Toiec.Control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.Toiec.model.DbConnectionInfo;
import com.example.Toiec.model.Id;
import com.example.Toiec.model.score;

/**
 * Servlet implementation class ToiecController
 */
//@WebServlet("/op/*")
public class ToeicController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /*
     * @see HttpServlet#HttpServlet()
     */
    public ToeicController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		
		//1. initialize for request processing
		HttpSession session = request.getSession(); //세션 객체 생성
		ToeicService service = (ToeicService)session.getAttribute("service"); //세선 테이블에서 서비스 불러오기
		if(service == null) { //서비스 없으면 만들기
			DbConnectionInfo connInfo = (DbConnectionInfo)getServletContext().getAttribute("connection_info");
			service = new ToeicService(connInfo);
			session.setAttribute("service", service);
		}
		
		//2. dispatch request to processing component
		String pathInfo = request.getPathInfo();
		String viewName = "error.jsp"; //에러로 바로 알아서 가도록 하기 위한 장치
		
		if(pathInfo.equals("/register")) { //index.jsp에서 회원가입 버튼 누르면
			viewName = "register.jsp";
		}
		else if(pathInfo.equals("/register.do")) {
			String email = request.getParameter("email");
			String pwd = request.getParameter("pwd");
			String name = request.getParameter("name");
			String phone = request.getParameter("phone");
			int score = 0; //임시 값 ㅋㅋ
			
			Id id_i = new Id(); //임시로 만든 id 객체.
			
			id_i.setEmail(email);
			id_i.setName(name);
			id_i.setPhone(phone);
			id_i.setPwd(pwd);
			id_i.setScore(score);
			
			service.registerId(id_i); //데이터베이스에 탑재!!
			
			viewName = "registerdo.jsp";
		}
		else if(pathInfo.equals("/test")) {
			viewName = "test1.jsp";
		}
		else if(pathInfo.equals("/testing1")) {
			String email = request.getParameter("email");
			String pwd = request.getParameter("pwd");
			
			Id id = service.searchId(email, pwd); //코드를 짜놔서 Id 형 객체가 나옴
			
			if(id != null) { //로그인 안 될경우 튕기는 페이지 만들기
				session.setAttribute("id", id);
				
				//목표 다음 문제를 불러오기.
				score score = new score();
				String question = null;
				
				score = service.getQuestion(1); //1번 문제 불러오기
				question = score.getQuestion();
				session.setAttribute("question1", question);
				
				score = service.getQuestion(2);
				question = score.getQuestion();
				session.setAttribute("question2", question);
				
				score = service.getQuestion(3);
				question = score.getQuestion();
				session.setAttribute("question3", question);
				
				viewName = "test2.jsp";
			}
			else {
				viewName = "notlogin.jsp";
			}
		}
		else if(pathInfo.equals("/testing2")) { //testscore 세션 속성 만들어지는 부분
			int testscore = 0;
			
			for(int i = 1 ; i < 4 ; i++) {
				if(1 == Integer.parseInt(request.getParameter(Integer.toString(i)))) {
					testscore++;
				}
			}
			
			session.setAttribute("testscore", testscore); //세션 테이블에 맞은 갯수 저장
			
			score score = new score();
			String question = null;
			
			score = service.getQuestion(4); 
			question = score.getQuestion();
			session.setAttribute("question4", question);
			
			score = service.getQuestion(5);
			question = score.getQuestion();
			session.setAttribute("question5", question);
			
			score = service.getQuestion(6);
			question = score.getQuestion();
			session.setAttribute("question6", question);
			
			viewName = "test3.jsp";
		}
		else if(pathInfo.equals("/testing3")) {
			int testscore = (int)session.getAttribute("testscore");
			
			for(int i = 4 ; i < 7 ; i++) {
				if(1 == Integer.parseInt(request.getParameter(Integer.toString(i)))) {
					testscore++;
				}
			}
			
			session.setAttribute("testscore", testscore); //세션 테이블에 맞은 갯수 저장
			
			score score = new score();
			String question = null;
			
			score = service.getQuestion(7); 
			question = score.getQuestion();
			session.setAttribute("question7", question);
			
			score = service.getQuestion(8);
			question = score.getQuestion();
			session.setAttribute("question8", question);
			
			score = service.getQuestion(9);
			question = score.getQuestion();
			session.setAttribute("question9", question);
			
			viewName = "test4.jsp";
		}
		else if(pathInfo.equals("/testing4")) {
			int testscore = (int)session.getAttribute("testscore");
			
			for(int i = 7 ; i < 10 ; i++) {
				if(1 == Integer.parseInt(request.getParameter(Integer.toString(i)))) {
					testscore++;
				}
			}
			
			session.setAttribute("testscore", testscore); //세션 테이블에 맞은 갯수 저장
			
			score score = new score();
			String question = null;
			
			score = service.getQuestion(10); 
			question = score.getQuestion();
			session.setAttribute("question10", question);
			
			score = service.getQuestion(11);
			question = score.getQuestion();
			session.setAttribute("question11", question);
			
			score = service.getQuestion(12);
			question = score.getQuestion();
			session.setAttribute("question12", question);
			
			viewName = "test5.jsp";
		}
		else if(pathInfo.equals("/testing5")) {
			int testscore = (int)session.getAttribute("testscore");
			
			for(int i = 10 ; i < 13 ; i++) {
				if(1 == Integer.parseInt(request.getParameter(Integer.toString(i)))) {
					testscore++;
				}
			}
			
			session.setAttribute("testscore", testscore); //세션 테이블에 맞은 갯수 저장
			
			score score = new score();
			String question = null;
			
			score = service.getQuestion(13); 
			question = score.getQuestion();
			session.setAttribute("question13", question);
			
			score = service.getQuestion(14);
			question = score.getQuestion();
			session.setAttribute("question14", question);
			
			score = service.getQuestion(15);
			question = score.getQuestion();
			session.setAttribute("question15", question);
			
			viewName = "test6.jsp";
		}
		else if(pathInfo.equals("/testing6")) {
			int testscore = (int)session.getAttribute("testscore");
			
			for(int i = 13 ; i < 16 ; i++) {
				if(1 == Integer.parseInt(request.getParameter(Integer.toString(i)))) {
					testscore++;
				}
			}
			
			session.setAttribute("testscore", testscore); //세션 테이블에 맞은 갯수 저장
			
			Id id = (Id)session.getAttribute("id");
			
			id.setScore(testscore);
			
			service.updateScore(id);
			
			viewName = "test7.jsp";
		}
		else if(pathInfo.equals("/score")) {
			viewName = "score.jsp";
		}
		else if(pathInfo.equals("/scoredo")) {
			String email = request.getParameter("email");
			String pwd = request.getParameter("pwd");
			
			Id id = service.searchId(email, pwd); //코드를 짜놔서 Id 형 객체가 나옴
			
			session.setAttribute("id", id);
			
			viewName = "scoredo.jsp";
		}
		
		//3. forward to view component
		StringBuilder sb = new StringBuilder(viewName);
		sb.insert(0, "../");
		RequestDispatcher view = request.getRequestDispatcher(sb.toString());
		view.forward(request, response);
	}

}

package com.example.Toiec.Listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.example.Toiec.model.DbConnectionInfo;

public class ToeicListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContextListener.super.contextInitialized(sce);
        
        ServletContext context = sce.getServletContext();
        
        DbConnectionInfo connInfo = new DbConnectionInfo();
        
        connInfo.setDriver(context.getInitParameter("jdbc_driver"));
        connInfo.setUrl(context.getInitParameter("db_url"));
        connInfo.setUser(context.getInitParameter("db_user"));
        connInfo.setPwd(context.getInitParameter("db_passwd"));
        connInfo.setDbIdTable(context.getInitParameter("db_idtable"));
        connInfo.setDbTestTable(context.getInitParameter("db_testtable"));
        
        context.setAttribute("connection_info", connInfo);
	}
	
	
}

package com.example.Toiec.model;

public class DbConnectionInfo {
	private String driver;
	private String url;
	private String user;
	private String pwd;
	private String dbIdTable;
	private String dbTestTable;
	
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getDbIdTable() {
		return dbIdTable;
	}
	public void setDbIdTable(String dbIdTable) {
		this.dbIdTable = dbIdTable;
	}
	public String getDbTestTable() {
		return dbTestTable;
	}
	public void setDbTestTable(String dbTestTable) {
		this.dbTestTable = dbTestTable;
	}
}

package com.example.Toiec.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ToeicDbJdbcOrigDao implements ToeicDbDao {
	private DbConnectionInfo connInfo = null;
	private Connection conn = null;
	private Statement stmt = null;
	
	public ToeicDbJdbcOrigDao(DbConnectionInfo connInfo) {
		this.connInfo = connInfo;
		
		try {
			Class.forName(connInfo.getDriver());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void connect() {
		try {
			conn = DriverManager.getConnection(connInfo.getUrl(), connInfo.getUser(),connInfo.getPwd());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void disconnect() {
		try {
			if(stmt != null) {
				stmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ArrayList<Id> getId(String email, String pwd) {
		ArrayList<Id> ids = null;
		
		connect();
		
		StringBuilder sb =new StringBuilder("select * from ");
		sb.append(connInfo.getDbIdTable());
		sb.append(" where email = '");
		sb.append(email);
		sb.append("' and pwd = '");
		sb.append(pwd);
		sb.append("';");
		
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sb.toString());
			
			if(rs.isBeforeFirst()) {
				ids = new ArrayList<Id>();
				while(rs.next()) {
					Id id = new Id();
					
					id.setEmail(rs.getString("email"));
					id.setName(rs.getString("name"));
					id.setPhone(rs.getString("phone"));
					id.setPwd(rs.getString("pwd"));
					id.setScore(rs.getInt("score")); //아마 0이 들어가겠죠?
					
					ids.add(id);
				}
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			disconnect();
		}
		
		return ids;
	}

	@Override
	public ArrayList<score> getTest(int test_id) {
		ArrayList<score> scorelist = null;
		
		connect();
		
		StringBuilder sb = new StringBuilder("select * from ");
		sb.append(connInfo.getDbTestTable());
		sb.append(" where test_id = ");
		sb.append(test_id);
		sb.append(";");
		
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sb.toString());
			if(rs.isBeforeFirst()) {
				scorelist = new ArrayList<score>();
				score score = new score();
				
				while(rs.next()) {
					score.setTest_id(rs.getInt("test_id"));
					score.setQuestion(rs.getString("question"));
					score.setAnswer(rs.getString("answer"));
					
					scorelist.add(score);
				}
			}
			rs.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			disconnect();
		}
		return scorelist;
	}

	@Override
	public int registerId(Id id) {
		int result = 0;
		
		connect();
		
		String sql = String.format("insert into %s (email, pwd, name, phone, score) values ('%s','%s','%s','%s','%d');", connInfo.getDbIdTable(), id.getEmail(),id.getPwd(), id.getName(), id.getPhone(), id.getScore());
		
		try {
			stmt = conn.createStatement();
			result = stmt.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = -1;
		}
		finally {
			disconnect();
		}
		return result;
	}
	
	@Override
	public int updateTest(Id id) {
		int result = 0;
		
		connect();
		
		String sql = String.format("update %s set score = %d where email = '%s';", connInfo.getDbIdTable(), id.getScore(), id.getEmail());
		
		try {
			stmt = conn.createStatement();
			result = stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			result = -1;
		}
		finally {
			disconnect();
		}
		return result;
	}

}

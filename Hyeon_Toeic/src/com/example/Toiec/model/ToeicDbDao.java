package com.example.Toiec.model;

import java.util.ArrayList;

public interface ToeicDbDao {
	public void connect(); //db 접속
	public void disconnect(); //db 접속 해제
	public ArrayList<Id> getId(String email, String pwd); //id 가져오기
	public ArrayList<score> getTest(int test_id); //문제 가져오기
	public int registerId(Id id); //id 만들기
	public int updateTest(Id id); //점수 업데이트 및 수정용
}

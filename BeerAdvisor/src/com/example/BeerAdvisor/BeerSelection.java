package com.example.BeerAdvisor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class BeerSelection
 */
//@WebServlet("/BeerSelection")
public class BeerSelection extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BeerSelection() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* version 1.0
		//1. get input parameters
		request.setCharacterEncoding("UTF-8");
		String color = request.getParameter("color");
		
		
		//2. data processing 
		
		
		//3. output processing results (jsp)
		response.setCharacterEncoding("UTF-8");
		
		PrintWriter out = response.getWriter();
		
		out.println("당신이 선택한 색깔은 " + color + "입니다.");
		*/
		/*
		//version 2.0
		//1. get input parameters
		request.setCharacterEncoding("UTF-8");
		String color = request.getParameter("color");
				
				
		//2. data processing 
		BeerExpert expert = new BeerExpert();
		List<String> brands = expert.getBrands(color);
		
				
		//3. output processing results (jsp)
		response.setCharacterEncoding("UTF-8");
				
		PrintWriter out = response.getWriter();
			
		out.println("당신이 선택한 색깔 " + color + "입니다.");
		out.println("당신의 원하는 맥주 리스트 ");
		for(int i = 0 ; i<brands.size(); i++) {
			out.println(brands);
		}
		*/
		
		//version 3.0
		//1. get input parameters
		request.setCharacterEncoding("UTF-8");
		
		String color = request.getParameter("color");
						
						
		//2. data processing 
		BeerExpert expert = new BeerExpert();
		List<String> brands = expert.getBrands(color);
				
						
		//3. output processing results (jsp)
		//어트리뷰트 테이블에 넣어주는 값이다. hash map 으로 되어있다.
		request.setAttribute("result", brands);
		
		//jsp 페스명을 주면서 넘기는 것이다.
		RequestDispatcher view = request.getRequestDispatcher("BrandListOutput.jsp");
		
		//포워딩할 파일을 넣어준다.
		view.forward(request, response);
	}

}

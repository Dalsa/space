package com.example.BeerAdvisor;

import java.util.ArrayList;
import java.util.List;

public class BeerExpert {
	
	public List<String> getBrands(String color) {
		//리스트에 string만 들어가게 하는...
		ArrayList<String> brands = new ArrayList<String>();
		
		if (color.equals("amber")) {
			brands.add("Jack Amber");
			brands.add("Red Moose");
		}
		else {
			brands.add("Jail Pale Ale");
			brands.add("Gout Stout");
		}
		return brands;
	}
}

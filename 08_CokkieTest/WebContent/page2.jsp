<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.example.sessiontest.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>세션 실험실</title>
</head>
<body>
	<%
		MyFavoriteInfo mfi = (MyFavoriteInfo)session.getAttribute("mfi");
	%>
	<div align = 'center'>
		<h1>좋아하는 동물</h1>
		<div align = 'right'>
			<p>안녕하세요, <%= mfi.getUserName() %> 님!</p>
		</div>
		<form method = "post" action = "page3.do">
			<p>당신이 좋아하는 동물?
			<select name = "animal">
				<option>백호</option>
				<option>청룡</option>
				<option>주작</option>
			</select></p>
			<p><input type = "submit" value = "고뢔" /></p>
		</form>
	</div>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.example.sessiontest.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>세션 실험실</title>
</head>
<body>
	<%
		MyFavoriteInfo mfi = (MyFavoriteInfo)session.getAttribute("mfi");
	%>
	<div align = 'center'>
		<h1>좋아하는 과목</h1>
		<div align = 'right'>
			<p>안녕하세요, <%= mfi.getUserName() %> 님!</p>
		</div>
		<form method = "post" action = "page4.do">
			<p>당신이 좋아하는 과목?
			<select name = "subject">
				<option>임베디드</option>
				<option>JSP</option>
				<option>데이터베이스</option>
			</select></p>
			<p><input type = "submit" value = "고뢔" /></p>
		</form>
	</div>
</body>
</html>
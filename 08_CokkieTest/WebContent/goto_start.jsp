<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>이건 읽어본 사람은 없겠지?</title>
</head>
<body>
	<script>
		window.onload = function () {
			alert("첫 페이지부터 시작하여야 합니다. \n 저리 사라지세요.");
			window.location.href = "start.html";
		}
	</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.example.sessiontest.* , java.util.Set" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>세션 실험실</title>
</head>
<body>
	<%
		MyFavoriteInfo mfi = (MyFavoriteInfo)session.getAttribute("mfi");
	%>
	<div align = 'center'>
		<h1>너가 좋아하는 거</h1>
		<div align = 'right'>
			<p>안녕하세요, <%= mfi.getUserName() %> 님!</p>
		</div>
		<p>당신의 좋아하는 리스트 : </p>
		<%
			Set<String> names = mfi.getFavoriteItemNames(); //불러와서 사용하기
			for (String name : names) {
		%>
				<p><%= name %> = <%= mfi.getFavoriteItem(name) %>
		<%
			}
			session.invalidate(); //세션 암묵적 종료
		%>
		<br><br>
		<p><a href = "start2.html">처음으로 돌아가기</a></p>
	</div>
</body>
</html>
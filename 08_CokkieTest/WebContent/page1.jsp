<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.example.sessiontest.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>세션 실험실</title>
</head>
<body>
	<%
		MyFavoriteInfo mfi = (MyFavoriteInfo)session.getAttribute("mfi");
	%>
	<div align = 'center'>
		<%@ include file = "header.jsp" %>
		<form method = "post" action = "page2.do">
			<p>당신이 좋아하는 과일은?
			<select name = "fruit">
				<option>사과</option>
				<option>바나나</option>
				<option>포도</option>
			</select></p>
			<p><input type = "submit" value = "고뢔" /></p>
		</form>
	</div>

</body>
</html>
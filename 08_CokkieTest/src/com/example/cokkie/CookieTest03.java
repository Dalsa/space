package com.example.cokkie;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CookieTest03
 */
@WebServlet("/cokkie_03.do")
public class CookieTest03 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CookieTest03() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html");
		
		//교수님 쿠키를 완전히 먹어 버리자.
		
		Cookie[] cookies = request.getCookies(); //교수님 쿠키를 들고왔다.
		
		Cookie vcCookie = null;
		
		//제일 맛있어 보이는 쿠키를 꺼내기 위해서 for문으로 찾아보자.
		for(Cookie cookie : cookies) { //찾기 쉽게 이터레이터가 설정되어있다.
			if("visit_count".equals(cookie.getName())) {
				vcCookie = cookie;
				cookie.setMaxAge(3600); //쿠키 유통기간을 한시간으로 만들자.
			}
			if("user_level".equals(cookie.getName())) {
				cookie.setMaxAge(-1); //쿠키 유통기한을 얼려버림..?
			}
			if("color_pref".equals(cookie.getName())) {
				cookie.setMaxAge(0); //쿠키를 바로 먹기
			}
		}
		
		int count = Integer.parseInt(vcCookie.getValue());
		count++; //다른 쿠키를 반 쪼개서 나두자.
		
		PrintWriter out = response.getWriter();
		
		out.println("<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"<meta charset='utf-8'>\r\n" + 
				"<title>영주교수님 쿠키를 없애보자!!!</title>\r\n" + 
				"</head>\r\n" + 
				"<body>\r\n" + 
				"	<h1>쿠키 테스트</h1>\r\n" + 
				"	<div>\r\n" + 
				"		<p>사이트 방문 횟수 : " + count +  "</p>" +
				"		<p>쿠키 목록 : " );
				for (Cookie cookie : cookies) {
					out.println("쿠키 이름 = " + cookie.getName() + ", 쿠키 내용물 = " + cookie.getValue() + "<br>");
				}
				//쿠키를 추가해주자.
				vcCookie.setValue(String.valueOf(count)); //쿠키값 원래 수정 
				
				for (Cookie cookie : cookies) {
					response.addCookie(cookie);
				}
				
				response.addCookie(vcCookie);
				
				out.println("<br><br><br>");
		out.println(
				"		<p><a href = 'Cokkie.html'>쿠키 제거하기</a></p>\r\n" + 
				"	</div>\r\n" + 
				"</body>\r\n" + 
				"</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

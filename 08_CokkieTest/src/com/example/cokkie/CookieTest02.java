package com.example.cokkie;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CookieTest02
 */
@WebServlet("/cookie_02.do")
public class CookieTest02 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CookieTest02() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html");
		
		//��Ű�� �԰� ������ �̹� �ִ�. ��
		
		Cookie[] cookies = request.getCookies(); //���� ����ũ�鿡 �ִ� ��Ű�� ���� �Ա� ���ؼ� ��� �Դ�.
		
		Cookie vcCookie = null;
		
		//���� ���־� ���̴� ��Ű�� ������ ���ؼ� for������ ã�ƺ���.
		for(Cookie cookie : cookies) { //ã�� ���� ���ͷ����Ͱ� �����Ǿ��ִ�.
			if("visit_count".equals(cookie.getName())) {
				vcCookie = cookie;
				break; //�� ��Ű�� �Ծ���ȴ�.
			}
		}
		
		int count = Integer.parseInt(vcCookie.getValue());
		count++; //�ٸ� ��Ű�� �� �ɰ��� ������.
		
		PrintWriter out = response.getWriter();
		
		out.println("<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"<meta charset='utf-8'>\r\n" + 
				"<title>���ֱ����� ��Ű�� ���ĺ���</title>\r\n" + 
				"</head>\r\n" + 
				"<body>\r\n" + 
				"	<h1>��Ű �׽�Ʈ</h1>\r\n" + 
				"	<div>\r\n" + 
				"		<p>����Ʈ �湮 Ƚ�� : " + count +  "</p>" +
				"		<p>��Ű ��� : " );
				for (Cookie cookie : cookies) {
					out.println("��Ű �̸� = " + cookie.getName() + ", ��Ű ���빰 = " + cookie.getValue() + "<br>");
				}
				//���� �Ծ����� �Ϻ� ���˸� ���ؼ� �ٽ� ��Ű�� �����ڸ��� ����.
				vcCookie.setValue(String.valueOf(count));
				response.addCookie(vcCookie);
				
				out.println("<br><br><br>");
		out.println(
				"		<p><a href = 'cokkie_03.do'>��Ű �����ϱ�</a></p>\r\n" + 
				"	</div>\r\n" + 
				"</body>\r\n" + 
				"</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package com.example.cokkie;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CookieTest01
 */
@WebServlet("/cokkie_01.do")
public class CookieTest01 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CookieTest01() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html");
		
		//쿠키가 먹고 싶지만 사먹을 돈이 없으니 쿠키를 만들어서 클라이언트 쪽으로 주자. ㅠㅠ(나도 없는데 왜 줘야해)
		
		Cookie cookie = new Cookie("visit_count", "1"); //쿠키를 만들어주기
		response.addCookie(cookie);
		
		cookie = new Cookie("user_level", "beginner");
		response.addCookie(cookie);
		
		cookie = new Cookie("color_pref", "black");
		response.addCookie(cookie);
		
		PrintWriter out = response.getWriter();
		
		out.println("<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"<meta charset='utf-8'>\r\n" + 
				"<title>영주교수님 내놓으세요.</title>\r\n" + 
				"</head>\r\n" + 
				"<body>\r\n" + 
				"	<h1>쿠키 테스트</h1>\r\n" + 
				"	<div>\r\n" + 
				"		<p>사이트 방문 횟수 : 1 </p>" +
				"		<p><a href = 'cookie_02.do'>쿠키 수정하기</a></p>\r\n" + 
				"	</div>\r\n" + 
				"</body>\r\n" + 
				"</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package com.example.sessiontest;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class PageDispatchController
 */
@WebServlet("/op/*") //이 밑에 있는 아이들 모든 것들이 서블릿이 매핑하도록
public class PageDispatchController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PageDispatchController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		
		//step 1. get URL path info
		String pathInfo = request.getPathInfo();
		
		MyFavoriteInfo mfi = null;
		String  viewName = null;
		
		HttpSession session = request.getSession();
		mfi = (MyFavoriteInfo)session.getAttribute("mfi");
		
		//step 2. route each URL path to processing routine
		if(pathInfo.contentEquals("/page1.do")) {
			if(mfi == null) {
				mfi = new MyFavoriteInfo();
				session.setAttribute("mfi", mfi);
			}
			String userName = request.getParameter("uesr_name");
			if(userName == null) {
				userName = "수상한 손님";
				mfi.setUserName(userName);
				
				viewName = "page1.jsp";
			}
		}
		else if(pathInfo.contentEquals("/page2.do")) {
			if(mfi != null) {
				String fruit = request.getParameter("fruit");
				mfi.addFavoriteItem("fruit", fruit);
				viewName = "page2.jsp";
			}
			else {
				viewName = "goto_start.jsp";
			}
		}
		else if(pathInfo.contentEquals("/page3.do")) {
			if(mfi != null) {
				String animal  = request.getParameter("animal");
				mfi.addFavoriteItem("animal", animal);
				viewName = "page3.jsp";
			}
			else {
				viewName = "goto_start.jsp";
			}
		}
		else if(pathInfo.contentEquals("/page4.do")) {
			if(mfi != null) {
				String subject = request.getParameter("subject");
				mfi.addFavoriteItem("subject", subject);
				viewName = "result.jsp";
			}
			else {
				viewName = "goto_start.jsp";
			}
		}
		
		//step 3. forward to view page
		if (viewName == null) {
			viewName = "goto_start.jsp";
		}
		StringBuilder sb = new StringBuilder(viewName);
		sb.insert(0, "../"); //첫번쨰 위치에 이 값을 추가하라는 의미
		
		RequestDispatcher view = request.getRequestDispatcher(sb.toString()); //이거 상대 페스여서 에러 뜸. 교수님이 톰캣 서버 날릴려고 준비했음.
		view.forward(request, response);
		
		/*
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		out.println("<p>" + request.getContextPath() + "</p>");
		out.println("<p>" + request.getPathInfo() + "</p>");
		out.println("<p>" + request.getRequestURI() + "</p>");
		out.println("<p>" + request.getRequestURL() + "</p>");
		*/
		
	}

}

package com.example.sessiontest;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MyFavoriteInfo {
	private String UserName;
	private Map<String, String> FavoriteList = null;
	
	//자기의 해쉽 맵을 만들어주는 기능
	public MyFavoriteInfo() {
		FavoriteList = new HashMap<String, String>();
	}
	
	//사용자 이름 입력 받고 출력
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	//해쉬 맵에 좋아하는 목록 추가하기 
	void addFavoriteItem(String name, String value) {
		FavoriteList.put(name, value);
	}
	//해쉬 맵에 좋아하는 목록 뽑아오는 기능
	public String getFavoriteItem(String name) { //이상하게 jsp 불러올때 public 되야한다.
		return FavoriteList.get(name);
	}
	
	public Set<String> getFavoriteItemNames() {
		return FavoriteList.keySet();
	}
}

package com.example.sessiontest;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Page1Control
 */
@WebServlet("/page1.do")
public class Page1Control extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Page1Control() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		
		//1. get input parameter
		String userName = request.getParameter("user_name");
		if(userName == null) {
			userName = "너는 누구야?";
		}
		HttpSession session = request.getSession(); //세션 아이디가 발급이 된다. 첫 서블릿이기 때문에 세션을 만들어주기로 했습니다.
		MyFavoriteInfo mfi = new MyFavoriteInfo(); //기존에 만든 클래스의 메소드이다.
		mfi.setUserName(userName); //세션 객체의 어트리뷰트 테이블에다가 등록해놓는다.
		
		session.setAttribute("mfi", mfi); //내가 가장 좋아하는 값을 넣어준다.
		
		//2. data processing
		
		//3. output results
		RequestDispatcher view = request.getRequestDispatcher("page1.jsp"); //page1.jsp 파일로 넘겨준다.
		view.forward(request, response); //jsp로 객체 포워딩
	}

}

package com.example.sessiontest;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Page3Control
 */
@WebServlet("/page3.do")
public class Page3Control extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Page3Control() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		
		//1. get input parameter
		String animal = request.getParameter("animal");
		if(animal == null) {
			animal = "없구나";
		}
		
		//2. data processing
		String viewName = null;
		HttpSession session = request.getSession(false); //기존의 세션이 있으면 반환하고 없으면 null을 반환한다.
		if(session == null) {
			viewName = "goto_start.jsp";
		}
		else {
			MyFavoriteInfo mfi = (MyFavoriteInfo) session.getAttribute("mfi"); //세션의 속성 테이블의 참조값을 가져온다.
			mfi.addFavoriteItem("animal", animal);
			viewName = "page3.jsp";
		}
		
		//3. output results
		RequestDispatcher view = request.getRequestDispatcher(viewName);
		view.forward(request, response); //jsp로 객체 포워딩s
	}

}

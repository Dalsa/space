<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="SimpleTags" prefix="st" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
</head>
<body>
	<c:set var="title" value="Simple Tag Test"/>
	<st:simple1/>
	<br><br>
	
	<st:simple2>
		이건 간단한 태그로 이루어진 바디 콘텐츠이다.<br>
		Title is ${title}
	</st:simple2>
	<br><br>
	
	<st:simple3 color="#9900aa">
		이건 간단한 태그로 이루어진 바디 콘텐츠이다.<br>
		그거 말고도 속성도 있는게 함정이다. <br>
		Title is ${title}
	</st:simple3>
	
</body>
</html>
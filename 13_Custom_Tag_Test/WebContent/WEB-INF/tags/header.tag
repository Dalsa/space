<%@ tag language="java" pageEncoding="utf-8"%>
<%@ attribute name = "subTitle" required="true" rtexprvalue = "true"%>
<%@ attribute name = "color" required="true" rtexprvalue = "true" %>
<%@ tag body-content="scriptless" %>
<img src="${pageContext.request.contextPath}/resources/gogi.jpg"/><br>
<br>
<em><b>${subTitle}</b></em>
<div style = "color: ${color};">
	<jsp:doBody />
</div>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="javax.servlet.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Visit Count</title>
</head>
<body>
<%-- 
	<%
		out.println("test output<br>");
		out.flush();
	%>
	<%
		RequestDispatcher view = request.getRequestDispatcher("header.jsp");
		view.forward(request,response);
	%>

	<%
		pageContext.forward("header.jsp");
	%>
--%>
	<jsp:forward page="header.jsp"/>

	<%!
		public void jspInit() {
			String email = getServletConfig().getInitParameter("email");
			getServletContext().setAttribute("email", email); //선언문 안에서는 jsp 내장 변수를 사용 할 수 없다.
		}
	%>
	<%!
		int count = 0; //멤버 변수나 멤버 함수로 할수 있는 declaration.
	%>
	<%!
		int doubleCount() {
			return (++count) * 2;
		}
	%>
	<h1>visit count </h1>
	<p>방문 횟수 : <%= doubleCount() %></p>
	<p>관계자의 파일 : <%= application.getAttribute("email") %></p>
</body>
</html>
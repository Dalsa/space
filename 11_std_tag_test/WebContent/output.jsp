<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import = "com.example.stdtagtest.domain.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>고용인 정보 등록 결과</title>
</head>
<body>
	<div align = "center">
		<h1>고용인 정보 등록</h1>
		<hr><br>
		<p>다음과 같은 정보가 등록되었습니다
<%-- 
	<%
		Person person = (Person)request.getAttribute("person");
	%>
	
			<table border="1">
				<tr>
					<td>이름</td>
					<td><%= person.getName() %></td>
				</tr>
				<tr>
					<td>성별</td>
					<td>
						<%= person.getGender() %>
					</td>
				</tr>
				<tr>
					<td>나이</td>
					<td>
						<%= person.getAge() %>
					</td>
				</tr>
			</table><br>
--%>
<%-- 
	<jsp:useBean id="person" type="com.example.stdtagtest.domain.Person" scope="request"/>
		<table border="1">
				<tr>
					<td>이름</td>
					<td><jsp:getProperty property="name" name="person"/></td>
				</tr>
				<tr>
					<td>성별</td>
					<td>
						<jsp:getProperty property="gender" name="person"/>
					</td>
				</tr>
				<tr>
					<td>나이</td>
					<td>
						<jsp:getProperty property="age" name="person"/>
					</td>
				</tr>
--%>
<table border="1">
				<tr>
					<td>이름</td>
					<td>${person.name }</td> <%-- bean 타입으로 정해놔서 가능한 이야기 --%>
				</tr>
				<tr>
					<td>성별</td>
					<td>
						${person.gender}
					</td>
				</tr>
				<tr>
					<td>나이</td>
					<td>
						${requestScope["person"]["age"]}
					</td>
				</tr>
				<tr>
					<td>업무</td>
					<td>
						${requestScope.person["task"]}
					</td>
				</tr>
		</table><br>
			<input type ="button" value="첫페이지" onclick="window.location.href='${pageContext.request.contextPath}/input.html';"/>
	</div>
</body>
</html>
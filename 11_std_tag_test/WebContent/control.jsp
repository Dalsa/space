<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.example.stdtagtest.domain.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>오우</title>
</head>
<body>
	<%
		request.setCharacterEncoding("utf-8");
	%>
	<%-- 
	<%
		Person person = new Person();
		person.setName(request.getParameter("name"));
		person.setGender(request.getParameter("gender"));
		person.setAge(Integer.parseInt(request.getParameter("age")));
		
		request.setAttribute("person", person);
		
		//ReqestDispatcehr view = requetst.getRequestDispatcher("output.jsp");
		//view.forward(request,response);
	%>
	--%>
	<jsp:useBean id="person" class="com.example.stdtagtest.domain.Person" scope ="request" />
	
	<%-- 
	<jsp:setProperty name="person" property="name" value='<%= request.getParameter("name") %>' />
	<jsp:setProperty name="person" property="gender" value='<%= request.getParameter("gender") %>'/>
	<jsp:setProperty name="person" property="age" value='<%= Integer.parseInt(request.getParameter("age")) %>'/>
	--%>
	
	<%-- 
	<jsp:setProperty name="person" property="name" param="name" />
	<jsp:setProperty name="person" property="gender" param="gender"/>
	<jsp:setProperty name="person" property="age" param="age"/>
	--%>
	
	<jsp:setProperty property="*" name="person"/> <%--파라메타 이름이랑 input 이름이 같아야 똑같이 된다. --%>
	
	<jsp:forward page = "output.jsp" />
</body>
</html>
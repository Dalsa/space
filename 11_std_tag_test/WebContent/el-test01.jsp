<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="my" uri = "MyELFunctions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
</head>
<body>
	<h1>EL Function Test</h1>
	SQRT(100) = ${my:squareroot(100)}<br>
	Roll Dice = ${my:rollIt() }<br>
</body>
</html>
package com.example.stdtagtest.domain;
//순수하게 자바 빈의 사양을 따르고 있는 클래스

import java.time.LocalDate;

public class Person {
	private String name;
	private String gender;
	private int birthYear; //getAge, setAge로 임의로 바꿈 탄생년도를 적어서 나이값을 유추하기.
	
	public Person() {
		//그냥 기본적으로 정의할 필요는 없지만 일단 나둠
		//매개변수를 가지는 생성자
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return LocalDate.now().getYear() - birthYear;
	}

	public void setAge(int age) {
		//현재 시간 값을 읽어와서 나이값을 빼면 탄생년도가 된다.
		this.birthYear = LocalDate.now().getYear() - age;
	}
}

package com.example.stdtagtest.domain;

public class Employee extends Person {
	private String task;

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}
}

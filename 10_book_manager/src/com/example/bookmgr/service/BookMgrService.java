package com.example.bookmgr.service;

//이 파일은 database를 받아와서 처리하는 기능이 구현된 프로그램 코드이다.
//책 리스트를 가져오는 메소드, 책 추가하는 메소드, 책 검색하는는 메소드,
//지우는 메소드, 수정하는 메소드 등이 있다.
//bookdbdao 인터페이스의 객체를 생성해서 사용한다.

import java.sql.SQLException;
import java.util.List;

import com.example.bookmgr.model.Book;
import com.example.bookmgr.model.BookDbDao;
import com.example.bookmgr.model.BookDbJdbcOrigDao;
import com.example.bookmgr.model.DbConnectionInfo;

public class BookMgrService {
	private BookDbDao dao = null;
	
	public BookMgrService(DbConnectionInfo connInfo) { // 메소드로 
		dao = new BookDbJdbcOrigDao(connInfo);
	}
	
	public List<Book> getBookList() { //책 리스트를 불러오는 기능
		try {
			return dao.getBooks("all");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public int insertBook(String code, String title, String writer, int price) { //db에 책 삽입하는 기능
		Book book = new Book(code, title, writer, price);
		
		int result = 0;
		
		try {
			result = dao.insertBook(book);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public Book searchBook(String code) throws SQLException {
		Book book = null;
		
		if (code != null || code != "") {
			List<Book> list = dao.getBooks(code);
			if (list != null) {
				book = list.get(0);
			}
		}
		
		return book;
	}
	
	public int deleteBook(String code) throws SQLException {
		int result = -1;
		
		if (code != null && code != "") {
			result = dao.deleteBook(code);
		}
		
		return result;
	}
	
	public int updateBook(Book book) throws SQLException {
		int result = -1;
		
		if (book != null) {
			result = dao.updateBook(book);
		}
		
		return result;
	}	
}


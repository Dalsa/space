package com.example.bookmgr.model;

//이 java 파일은 책에 대한 정보를 읽고 저장하며 출력하는 역할을 하는 기능을 구현되어 있다.

public class Book {
	private String code;
	private String title;
	private String writer;
	private int price;
	
	public Book() {
		
	}
	
	public Book(String code, String title, String writer, int price) {
		super();
		this.code = code;
		this.title = title;
		this.writer = writer;
		this.price = price;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}	
}

package com.example.bookmgr.model;

//인터페이스이다 db와 연동하고 db를 사용하는 프로그램을 한다.

import java.sql.SQLException;
import java.util.List;

public interface BookDbDao {
	public void connect() throws SQLException;
	public void disconnect() throws SQLException;
	public List<Book> getBooks(String code)  throws SQLException;
	public int insertBook(Book book)  throws SQLException;
	public int updateBook(Book book)  throws SQLException;
	public int deleteBook(String code) throws SQLException;	
}

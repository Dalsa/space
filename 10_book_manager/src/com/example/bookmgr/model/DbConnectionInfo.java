package com.example.bookmgr.model;
//이 자바 프로그램은 프로그램 불러오고 빠지는 driver = 드라이버 이름
//url 주소랑 , mysql 아이디랑 비밀번호, db 테이블 이름까지 

public class DbConnectionInfo {
	private String driver;
	private String url;
	private String user;
	private String passwd;
	private String dbTable;
	
	public String getDriver() {
		return driver;
	}
	
	public void setDriver(String driver) {
		this.driver = driver;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getUser() {
		return user;
	}
	
	public void setUser(String user) {
		this.user = user;
	}
	
	public String getPasswd() {
		return passwd;
	}
	
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	
	public String getDbTable() {
		return dbTable;
	}
	
	public void setDbTable(String dbTable) {
		this.dbTable = dbTable;
	}	
}

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, com.example.bookmgr.model.*" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>DB Connection Test</title>
</head>
<body>
	<p> Try to connect MySQL DB Server...</p>
	<%
		String msg = null;
		Connection conn = null;
		DbConnectionInfo connInfo = 
			(DbConnectionInfo)application.getAttribute("connection_info");
		
	    try {
/*
	    	Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/bookdb",
				"testuser", "1234");
*/
	    	Class.forName(connInfo.getDriver());
			conn = DriverManager.getConnection(
				connInfo.getUrl(),
				connInfo.getUser(), 
				connInfo.getPasswd());

	    }
	    catch (ClassNotFoundException|SQLException ex) {
	    	msg = ex.getMessage();
	    }
	    
		if (conn != null) {
			msg = "Succeed to connect MySQL Server...";
		}
		else {
			msg = "Failed to connect MySQL Server...";
		}
	%>
	<P><%= msg %></P>
	<%
		if (conn != null) {
			out.println("<p>Try to disconnect from MySQL Server...</p>");
			conn.close();
		}
	%>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>도서 상품 관리</title>
</head>
<body>
	<jsp:include page="/header.jsp" flush="true" />
	
	<div align="center" style="height: 480px;">
		<h2>도서 상품 관리</h2><br>
		요청한 서비스가 없거나 처리 과정에서 오류가 발생하였습니...<br>		
		<br><br>
		<input type="button" value="첫페이지 >>" 
			onclick="window.location.href='${pageContext.request.contextPath}/start.jsp';" />
		<br><br><br><br><br>	
	</div>
	
	<jsp:include page="/footer.jsp" flush="true"/>	
</body>
</html>
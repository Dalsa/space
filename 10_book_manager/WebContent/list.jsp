<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*, com.example.bookmgr.model.*" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>도서 전체 목록</title>
<style>
.th {
	text-align: center;
	background-color: LightSkyBlue;
}

.td {
	text-align: center;
}
</style>
</head>
<body>
	<jsp:include page="/header.jsp" flush="true" />
	
	<div align="center" style="height: 480px;">
		<h2>도서 전체 목록</h2><br>
		
<%
		List<Book> bookList = 
			(List<Book>)request.getAttribute("list");
%>		
		
		<form>
			<table border="1">
				<tr>
					<th class="th" width="75">코드</th>
					<th class="th" width="250">제목</th>
					<th class="th" width="150">저자</th>
					<th class="th" width="75">가격</th>
				</tr>
<%
			for (Book book : bookList) {
%>				
				<tr>
					<td class="td" width="75"><%= book.getCode() %></td>
					<td class="td" width="250"><%= book.getTitle() %></td>
					<td class="td" width="150"><%= book.getWriter() %></td>
					<td class="td" width="75"><%= book.getPrice() %></td>
				</tr>
<%
			}
%>				
			</table>
			<br><br>
			<input type="button" value="첫페이지 >>"
					onclick="window.location.href=
						'${pageContext.request.contextPath}/start.jsp';" >
		</form>
	</div>

	<jsp:include page="/footer.jsp" flush="true" />
</body>
</html>
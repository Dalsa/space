<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>생각보다 고기반찬</title>
</head>
<body>
	<c:set var = "currentTip" value="<b></b>" scope = "page" />
	<%-- page는 scope 기본 값입니다.--%>
	
	<div>
		<b>Tip of the Day:</b><br><br>
		<c:out value="${pageScope.currentTip}" escapeXml="true" /> tags make things bold! <br>
		Hello, <c:out value="${user}" default="Guest"/>
	</div>
	
	<c:remove var="currentTip" scope = "page" />
	<%-- remove를 하는데 scope를 하지 않으면 모든 것에 동일한 이름의 속성을 다 지워버린다.. --%>
</body>
</html>
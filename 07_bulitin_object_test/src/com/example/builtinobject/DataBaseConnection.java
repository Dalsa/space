package com.example.builtinobject;

public class DataBaseConnection {
	private String dbServer;
	private String dbUser;
	private String dbPassword;
	private String dbTable;
	
	public DataBaseConnection(String dbServer, String dbUser, String dbPassword, String dbTable) {
		super();
		this.dbServer = dbServer;
		this.dbUser = dbUser;
		this.dbPassword = dbPassword;
		this.dbTable = dbTable;
	}
	
	public String getConnection() {
		return "Connected...";
	}
	
	public String closeConnection() {
		return "Disconnected..";
	}
}

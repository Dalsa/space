package com.example.builtinobject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class imagetest
 */
@WebServlet("/image_test.do")
public class imagetest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public imagetest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("image/jpeg"); //이미지 보낼꺼라서 그럼.
		
		ServletContext context = getServletContext();
		InputStream is = context.getResourceAsStream("resources/for.jpg");
		
		int readCnt = 0;
		byte[] buffer = new byte[1024];
		OutputStream os = response.getOutputStream();
		while ((readCnt = is.read(buffer)) != -1) { //데이터가 없으면 -1이기 때문임
			os.write(buffer, 0, readCnt);
		}
		os.flush();
		os.close();
		is.close();
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

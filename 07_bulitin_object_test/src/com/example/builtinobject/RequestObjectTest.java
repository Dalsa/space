package com.example.builtinobject;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RequestObjectTest
 * 좀 만들어 봐라 동현아
 */
@WebServlet("/request_test.do")
public class RequestObjectTest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RequestObjectTest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		out.println("<html>");
			out.println("<head>");
				out.println("<meta charset = 'utf-8'>");
				out.println("<title>리퀘스트 오브젝트 테스트</title>");
			out.println("</head>");
			out.println("<body>");
				out.println("<h1>Request Object Test</h1><hr>");
				out.println("<ol>");
					out.println("<li>요청 URL : " + request.getRequestURL() + "</li>");
					out.println("<li>요청 URI : " + request.getRequestURI() + "</li>");
					out.println("<li>요청 메소드 : " + request.getMethod() + "</li>");
					out.println("<li>쿼리 스트링 : " + request.getQueryString() + "</li>");
					out.println("<li>헤드 이름 목록  : ");
					Enumeration<String> headers = request.getHeaderNames(); //enumeration 이터레이션 타입의 for문을 사용할 수 없다.
					while (headers.hasMoreElements()) {
						out.print(headers.nextElement() + ", ");
					}
					out.println("</li>");
					out.println("<li>브라우저 유형  : " + request.getHeader("user-agent") + "</li>");
				out.println("</ol>");
			out.println("</body>");
		out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package com.example.builtinobject;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class config
 */
//@WebServlet("/config_test.do")
public class config extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public config() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		out.println("<html>");
			out.println("<head>");
				out.println("<meta charset = 'utf-8'>");
				out.println("<title>서브릿 컨피그 테스트</title>");
			out.println("</head>");
			out.println("<body>");
				out.println("<h1>Servlet Config Object Test</h1><hr>");
				out.println("<ol>");
					out.println("<li>초기화 파라미터 목록  : ");
					ServletConfig config = getServletConfig();
					Enumeration<String> params = config.getInitParameterNames();
					while(params.hasMoreElements()) {
						out.println(params.nextElement() + ", ");
					}
					out.println("</li>");
					out.println("<li>이메일 주소  : " + config.getInitParameter("email_address") + "</li>");
					out.println("<li>DB 유저  : " + config.getInitParameter("db_table") + "</li>");
					out.println("<li>User Count  : " + Integer.parseInt(config.getInitParameter("user_count")) + "</li>");
				out.println("</ol>");
			out.println("</body>");
		out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

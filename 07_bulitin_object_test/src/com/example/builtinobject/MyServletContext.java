package com.example.builtinobject;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyServletContext implements ServletContextListener {
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		ServletContextListener.super.contextInitialized(sce);
		
		//서블릿컨텍스트의 전역의 파일을 넣고
		ServletContext context = sce.getServletContext();
		String dbServer = context.getInitParameter("db_server");
		String dbUser = context.getInitParameter("db_user");
		String dbPasswd = context.getInitParameter("db_passwd");
		String dbTable = context.getInitParameter("dbs");
		
		//처리되는 하드 코딩 모듈은 따로 api 식으로 만들놓음.
		DataBaseConnection conn = new DataBaseConnection(dbServer, dbUser, dbPasswd, dbTable);
		
		//컨텍스트 어트리뷰트 테이블에 등록해놓기.
		context.setAttribute("db_connection", conn);
		
		//테스트에서 해보기 위해서 만드는 문장.
		System.out.println("INFO >> " + conn.getConnection());
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		ServletContextListener.super.contextDestroyed(sce);
		
		ServletContext context = sce.getServletContext();
		DataBaseConnection conn = (DataBaseConnection)context.getAttribute("db_connection");
		
		if (conn != null) {
			conn.closeConnection(); //닫아주기
			System.out.println("Info >> " + conn.closeConnection());
		}
	}
	
}

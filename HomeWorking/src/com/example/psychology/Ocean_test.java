package com.example.psychology;

import java.util.HashMap;
import java.util.Map;

public class Ocean_test {
	private Map<String, Integer> Test = null; //테스트 결과를 맵에 넣음.
	
	//테스트 해쉬맵을 만들어 주는 기능.
	public Ocean_test() {
		Test = new HashMap<String, Integer>();
	}
	
	//해쉬 맵에 좋아하는 목록 추가하기 
	public void addTest(String name, Integer value) {
		Test.put(name, value);
	}
	//해쉬 맵에 좋아하는 목록 뽑아오는 기능
	public Integer getTest(String name) { //이상하게 jsp 불러올때 public 되야한다.
		return Test.get(name);
	}
	
	//개방성
	public int Openness() {
		int openness = getTest("ten") + getTest("five");
		
		return openness;
	}
	
	//성실성
	public int Conscientiousness() {
		int sincerity = 8 - getTest("eight") + getTest("three");
		
		return sincerity;
	}
	
	//외향성
	public int Extroversions() {
		int extrovert = 8 - getTest("six") + getTest("one");
			
		return extrovert;
	}
	
	//동조성
	public int Agreeableness() {
		int sympathy = 8 - getTest("two") + getTest("seven");
		
		return sympathy;
	}
	
	//신경성
	public int Neuroticism() {
		int sensitivity = 8 - getTest("nine") + getTest("four");
		
		return sensitivity; 
	}
}

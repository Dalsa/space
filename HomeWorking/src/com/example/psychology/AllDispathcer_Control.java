package com.example.psychology;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AllDispathcer_Control
 */
//@WebServlet("/op/*")
public class AllDispathcer_Control extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AllDispathcer_Control() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		
		//url 경로를 구하기
		String pathInfo = request.getPathInfo();
		
		Ocean_test ot = null;
		String viewName = null;
		HttpSession session = null;
		
		session = request.getSession();
		
		//route request to service components
		if(pathInfo.contentEquals("/TestingStart.do")) {
			String userName = request.getParameter("user_name");
			session = request.getSession();
			ot = (Ocean_test)session.getAttribute("ot");
			
			if(ot == null) { //만약에 ot의 값이 없는 경우 새로운 ot hashmap을 넣는다.
				session.setAttribute("ot", new Ocean_test()); //참조값을 넣는것
			}
			session.setAttribute("user_name", userName); //유저네임도 세션어트리뷰트에 넣어주자.
			viewName = "test1.jsp";
		}
		else if(pathInfo.contentEquals("/testing1.do")) {
			session = request.getSession(false); //false 설정시 값 없으면 만들지 않고 null값을 적는다.
			
			if(session != null) {
				ot = (Ocean_test)session.getAttribute("ot"); //세션 값을 불러와서.
				
				String one = request.getParameter("one"); //파라미터 값을 불러드린다.
				String two = request.getParameter("two");
				String three = request.getParameter("three");
				
				ot.addTest("one", Integer.parseInt(one)); //값을 세션 속성테이블에 저장한다.
				ot.addTest("two", Integer.parseInt(two));
				ot.addTest("three", Integer.parseInt(three));
				
				
				viewName = "test2.jsp"; //다음 뷰로 가도록 구현한다.
			}
			else { //세션이 없을 경우 에러가 발생하도록 구현
				viewName = "goto_start.jsp";
			}
		}
		else if(pathInfo.contentEquals("/testing2.do")) {
			session = request.getSession(false);
			
			if(session != null) {
				ot = (Ocean_test)session.getAttribute("ot");
				
				String four = request.getParameter("four");
				String five = request.getParameter("five");
				String six = request.getParameter("six");
				
				ot.addTest("four", Integer.parseInt(four));
				ot.addTest("five", Integer.parseInt(five));
				ot.addTest("six", Integer.parseInt(six));
				
				viewName = "test3.jsp";
			}
			else {
				viewName = "goto_start.jsp";
			}
		}
		else if(pathInfo.contentEquals("/testing3.do")) {
			session = request.getSession(false);
			
			if(session != null) {
				ot = (Ocean_test)session.getAttribute("ot");
				String seven = request.getParameter("seven");
				String eight = request.getParameter("eight");
				String nine = request.getParameter("nine");
				
				ot.addTest("seven", Integer.parseInt(seven));
				ot.addTest("eight", Integer.parseInt(eight));
				ot.addTest("nine", Integer.parseInt(nine));
				
				viewName = "test4.jsp";
			}
			else {
				viewName = "goto_start.jsp";
			}
		}
		else if(pathInfo.contentEquals("/testing4.do")) {
			session = request.getSession(false);
			
			if(session != null) {
				ot = (Ocean_test)session.getAttribute("ot");
				
				String ten = request.getParameter("ten");
				
				ot.addTest("ten", Integer.parseInt(ten));
				
				viewName = "result.jsp";
			}
			else {
				viewName = "goto_start.jsp";
			}
		}
		else {
			viewName = "goto_start.jsp"; //아무것도 없을 시에는 이런식으로 보내버린다.
		}
		
		StringBuilder sb = new StringBuilder(viewName);
		sb.insert(0, "../"); //첫번쨰 위치에 이 값을 추가하라는 의미
		
		RequestDispatcher view = request.getRequestDispatcher(sb.toString()); //상대 경로인 것을 주의해야한다.
		view.forward(request, response);
		
	}

}

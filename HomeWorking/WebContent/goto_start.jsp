<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>이건 읽어본 사람은 없겠지?</title>
</head>
<body>
	<script>
		window.onload = function () {
			alert("처음 부터 시작해야하거나, 없는 페이지를 요청하셨습니다. \n 다시 시작하세요!");
			window.location.href = "/HomeWorking/index.jsp";
		}
	</script>
</body>
</html>
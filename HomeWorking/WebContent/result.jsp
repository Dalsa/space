<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import =  "com.example.psychology.Ocean_test" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>오션 파이브 심리테스트 결과</title>
</head>
<body>
<%
	Ocean_test ot = (Ocean_test)session.getAttribute("ot"); //세션 어트리뷰트 테이블의 오션 테스트 객체를 불러온다.
%>
	<div align = "center">
	<%@ include file = "header.jsp" %>
	<p>Ocean 심리테스트의 결과입니다.</p>
	<p>개방성 : <%= ot.Openness() %></p>
	<p>성실성 : <%= ot.Conscientiousness() %></p>
	<p>외향성 : <%= ot.Extroversions() %></p>
	<p>동조성 : <%= ot.Agreeableness() %></p>
	<p>신경성 : <%= ot.Neuroticism() %></p>
	<p>성격 유형의 평균 점수<br>
	개방성 남성 : 10.8 , 여성 : 10.7<br>
	성실성 남성 : 11.0 , 여성 : 10.4<br>
	외향성 남성 : 9.1 , 여성 : 8.5<br>
	동조성 남성 : 10.6, 여성 : 10.1<br>
	신경성 남성 : 6.7, 여성 : 5.7</p>
	<p>비교를 하기 위한 상대적인 점수입니다.</p>
	<p>점수가 낮거나 너무 높다고 해서 안 좋은 것이 아닙니다.</p>
	<p><a href = "/HomeWorking/index.jsp">다시 시험 치기</a></p>
	</div>
	<%@ include file = "contact_point.jsp" %>
	<% session.invalidate(); %>
</body>
</html>
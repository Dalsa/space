<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import =  "com.example.psychology.Ocean_test" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>오션 파이브 심리테스트 4</title>
</head>
<body>
	<div align = "center">
	<%@ include file = "header.jsp" %>
	<p>성심성의것 테스트 해주시길 바랍니다.</p>
	<p>완전히 부정한다 = 1, 다소 부정한다 = 2, 약간 부정한다 = 3, 동의하지도 부정하지도 않는다 = 4,<br>
	약간 동의한다 = 5, 다소 동의한다 = 6, 완전히 동의한다 = 7
	</p>
	<form method = "post" action = <%= response.encodeURL("testing4.do") %>>
		<p>10. 내 생각에 나는 전통적이며, 창의성이 없다.
		<select name = "ten">
			<option>1</option>
			<option>2</option>
			<option>3</option>
			<option>4</option>
			<option>5</option>
			<option>6</option>
			<option>7</option>
		</select>
		</p> 
		<p><input type = "submit" value = "결과" /></p>
	</form>
	</div>
	<%@ include file = "contact_point.jsp" %>
</body>
</html>